-- bonds_RanSu.sql

-- 1
select * from bond
where CUSIP = '28717RH95';

-- 2
select * from bond
order by maturity;

-- 3
select *, quantity*price as 'portfolio' from bond;

-- 4
select *, round(quantity*coupon/100, 2) as 'portfolio' from bond;

-- 5
select * from bond 
join rating on bond.rating = rating.rating
where rating.ordinal <=3

-- 6
select rating, avg(price) as 'Avg Price', avg(coupon) as 'Avg Coupon' 
from bond
group by rating;

-- 7
select *, round(coupon/price, 2) as 'Yield' 
from bond
inner join rating on bond.rating = rating.rating and coupon/price < rating.expected_yield
