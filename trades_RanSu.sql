-- trades_RanSu.sql

-- create all views (since CREATE VIEW mucst be the first statement in a query batch)
-- this view shows all trades by all traders (same as question 1)

create view tradersAndTrades
as
select distinct trader_ID, tr.first_name, tr.last_name, t.*, p.opening_trade_ID, p.closing_trade_ID,
case
		when buy = 1
			then -price*size
		else price*size
			end as 'Total'
from trade t
join position p on t.ID = p.opening_trade_ID or t.ID = p.closing_trade_ID
join trader tr on tr.ID =p.trader_ID

create view profitAndLoss 
as
select trader_ID, first_name, last_name, sum(Total) as 'profitAndLoss'
from tradersAndTrades
group by trader_ID, first_name, last_name;


-- 1
select distinct trader_ID, tr.first_name, tr.last_name, t.*, p.opening_trade_ID, p.closing_trade_ID from trade t
join position p on t.ID = p.opening_trade_ID or  t.ID = p.closing_trade_ID
join trader tr on tr.ID =p.trader_ID
where tr.ID = 1 and t.stock = 'MRK'			-- to specify a certain trader and stock
-- order by p.trader_ID, t.stock;


-- 2
select trader_ID, first_name, last_name, sum(Total) as 'profitAndLoss'
from tradersAndTrades
where trader_ID = 1	-- to select a specific trader
group by trader_ID, first_name, last_name


-- 3
select * from profitAndLoss


